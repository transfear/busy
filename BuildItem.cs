﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuSy
{
	internal class BuildItem
	{
		public class Dependency
		{
			public string   FilePath      { get; set; }
			public DateTime LastWriteTime { get; set; }
		}

		public string SrcFile { get; set; }
		public List<Dependency> DepList { get; } = new List<Dependency>();

		public BuildItem() { }
	}
}
