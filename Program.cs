﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BuSy.ProjectFile;

namespace BuSy
{
	class Program
	{
		static void Main(string[] args)
		{
#if DEBUG
			// Prompt for debugger if none is attached
			if (!System.Diagnostics.Debugger.IsAttached)
				System.Diagnostics.Debugger.Launch();
#endif

			if (args.Length < 1)
			{
				Usage();
				return;
			}

			try
			{
				CommandLineArgs cmdLine = new CommandLineArgs(args);
				Project         project = new Project(cmdLine);
				project.Parse();
				project.Execute();
			}
			catch (Exception e)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				while (e != null)
				{
					string error = String.Format("{0}: {1}\r\n{2}\r\n\r\n", e.Source, e.Message, e.StackTrace);
					Console.Error.WriteLine(error);
					System.Diagnostics.Debug.WriteLine(error);
					e = e.InnerException;
				}
				Console.ResetColor();
			}
		}

		static void Usage()
		{
			// TODO
		}
	}
}
