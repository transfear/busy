﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BuSy.ProjectFile
{
	public class DefaultOptions
	{
		[XmlElement("PreBuildStep", IsNullable = true)]
		public string PreBuildStep { get; set; }
		[XmlElement("PostBuildStep", IsNullable = true)]
		public string PostBuildStep { get; set; }

		[XmlElement("CommonOptions", IsNullable = true)]
		public string CommonOptions { get; set; } = String.Empty;
		[XmlElement("GenerateDepsOptions", IsNullable = true)]
		public string GenerateDepsOptions { get; set; }
		[XmlElement("GenerateIntOptions", IsNullable = true)]
		public string GenerateIntOptions { get; set; }
		[XmlElement("GenerateOutOptions", IsNullable = true)]
		public string GenerateOutOptions { get; set; }
		[XmlElement("CompileOptions", IsNullable = true)]
		public string CompileOptions { get; set; } = String.Empty;
		[XmlElement("LinkOptions", IsNullable = true)]
		public string LinkOptions { get; set; } = String.Empty;

		[XmlElement("AssemblyName", IsNullable = true)]
		public string AssemblyName { get; set; } 

		public DefaultOptions() { }
	}
}
