﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BuSy.ProjectFile
{
	public class ProjectProperties
	{
		public string ProjDir { get; set; } // Root folder of the whole project (not necessarily the folder in which the project file is located)

		[XmlElement("ParallelBuild", IsNullable = true)]
		public Nullable<bool> ParallelBuild { get; set; } = true;

		public string SrcDir     { get; set; }	// Folder where source files are located
		public string SrcPattern { get; set; }  // File pattern to identify files to build

		[XmlElement("SrcRecurse", IsNullable = true)]
		public Nullable<System.Boolean> SrcRecurse { get; set; } = true;	// Search in source subfolders as well

		public string DepsDir { get; set; }	// Folder where each file dependencies are stored. Should be per-target.

		public string IntDir     { get; set; }	// Folder where intermediate (obj files) are stored. Should be per target.
		public string IntPattern { get; set; }	// File pattern used to generate intermediate files

		public string OutDir     { get; set; } // Folder where final linked assembly is stored.
		public string OutPattern { get; set; } // File pattern used to generate final assembly

		public string Compiler { get; set; }	// Path to the compiler executable
		public string Linker { get; set; }		// Path to the linker executable
	}
}
