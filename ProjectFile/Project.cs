﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BuSy.ProjectFile
{
	internal class Project
	{
		public string Path { get; set; }
		public Root Root { get; set; }

		private readonly CommandLineArgs    cmdLine       = null;
		private Target                      currentTarget = null;
		private List<Tuple<string, string>> genericTokens = new List<Tuple<string, string>>();

		public Project(CommandLineArgs _args)
		{
			cmdLine = _args;
			Path = cmdLine.ProjPath;
		}

		public void Parse()
		{
			using (StreamReader sr = new StreamReader(Path))
			{
				XmlSerializer xmlSr = new XmlSerializer(typeof(Root));
				Root = (Root)xmlSr.Deserialize(sr);
			}
		}

		public void Execute()
		{
			ResolveTarget();
			SetProjDir();

			if ((cmdLine.Rule == Rule.Clean) || (cmdLine.Rule == Rule.Rebuild))
			{
				CleanDeps();
				CleanIntFiles();
			}

			if ((cmdLine.Rule == Rule.Build) || (cmdLine.Rule == Rule.Rebuild))
			{
				string szIntDir = GetProjectProperty("IntDir");
				if (!Directory.Exists(szIntDir))
					Directory.CreateDirectory(szIntDir);

				string szDepsDir = GetProjectProperty("DepsDir");
				if (!Directory.Exists(szDepsDir))
					Directory.CreateDirectory(szDepsDir);

				PreBuildStep();

				List<string>          toBuild     = FetchFilesToBuild();
				ConcurrentBag<string> filesToLink = new ConcurrentBag<string>();

				int numNewFiles = 0;
				if (Root.ProjectProperties.ParallelBuild.GetValueOrDefault())
				{
					Parallel.ForEach(toBuild, bi => BuildFile(bi, filesToLink, ref numNewFiles));
				}
				else
				{
					foreach (string bi in toBuild)
						BuildFile(bi, filesToLink, ref numNewFiles);
				}

				
				if (numNewFiles > 0)
				{
					if (cmdLine.Verbose)
						Console.WriteLine("Built {0} new files.", numNewFiles);
					LinkBuildItems(filesToLink);
				}
				else
				{
					Console.WriteLine("Nothing to build, everything is up to date.");
				}

				PostBuildStep();
			}
		}

		private void BuildFile(string _bi, ConcurrentBag<string> _filesToLink, ref int _numNewFiles)
		{
			string szIntFile = GetIntFileName(_bi);
			_filesToLink.Add(szIntFile);

			bool isDirty = true;

			// Compile build item if deps don't exist, or if dep file isn't up-to-date
			BuildItem deps      = FetchDependencies(_bi);
			string    szDepsDir = GetProjectProperty("DepsDir");
			if (Directory.Exists(szDepsDir))
			{
				string depFile = System.IO.Path.Combine(szDepsDir, deps.SrcFile);
				isDirty = !IsDepFileUpToDate(depFile, deps);
			}

			// Compile if intermediate file doesn't exist
			if (!File.Exists(szIntFile))
				isDirty = true;

			// File is up-to-date
			if (!isDirty)
			{
				if (cmdLine.Verbose)
					Console.WriteLine("{0} is not dirty, skipping", _bi);
				return;
			}

			CompileBuildItem(deps, szIntFile);
			Interlocked.Increment(ref _numNewFiles);
		}

		private string GetDepFileFromBuildItem(BuildItem _bi)
		{
			string depFileName = _bi.SrcFile;
			depFileName = depFileName.Replace('\\', '.');
			depFileName = depFileName.Replace('/', '.');
			depFileName += ".dep";

			string szDepsDir     = GetProjectProperty("DepsDir");
			string szDepFilePath = System.IO.Path.Combine(szDepsDir, depFileName);

			return szDepFilePath;
		}

		private void GenerateDepFile(BuildItem _bi)
		{
			string depFilePath = GetDepFileFromBuildItem(_bi);
			using (StreamWriter sw = new StreamWriter(depFilePath))
			{
				foreach (BuildItem.Dependency dep in _bi.DepList)
				{
					string depLine = String.Format("{0},{1}", dep.FilePath, dep.LastWriteTime.Ticks);
					sw.WriteLine(depLine);
				}
			}
		}

		private bool IsDepFileUpToDate(string _depFile, BuildItem _bi)
		{
			string depFilePath = GetDepFileFromBuildItem(_bi);
			if (!File.Exists(depFilePath))
				return false;

			Dictionary<string, long> depValues = new Dictionary<string, long>();
			using (StreamReader sr = new StreamReader(depFilePath))
			{
				string depLine = null;
				while ((depLine = sr.ReadLine()) != null)
				{
					int splitIdx = depLine.IndexOf(',');

					string szDependencyFile = depLine.Substring(0, splitIdx);
					string szDependencyDate = depLine.Substring(splitIdx + 1);

					long dt = 0;
					if (long.TryParse(szDependencyDate, out dt))
						depValues.Add(szDependencyFile, dt);
				}
			}

			foreach (BuildItem.Dependency curDep in _bi.DepList)
			{
				string fileToCheck = curDep.FilePath;

				// We have a new dependency?
				long oldDepTime;
				if (!depValues.TryGetValue(fileToCheck, out oldDepTime))
					return false;

				// Dependency has changed since last build?
				if (oldDepTime != curDep.LastWriteTime.Ticks)
					return false;
			}

			return true;
		}

		private void LinkBuildItems(ConcurrentBag<string> _builtFiles)
		{
			string szOutDir = GetProjectProperty("OutDir");
			if (!Directory.Exists(szOutDir))
				Directory.CreateDirectory(szOutDir);

			string szLinker          = GetProjectProperty("Linker");
			string szCommonOpts      = GetOverridableProperty("CommonOptions");
			string szLinkerOpts      = GetOverridableProperty("LinkOptions");
			string szGenerateOutOpts = GetOverridableProperty("GenerateOutOptions");
			string szOutPattern      = GetProjectProperty("OutPattern");
			string[] szIntFiles      = _builtFiles.ToArray();

			string szProcAndArgs = String.Format("{0} {1} {2} {3} {4} {5}", szLinker, szCommonOpts, szLinkerOpts, string.Join(" ", szIntFiles), szGenerateOutOpts, szOutPattern);
			Console.WriteLine(String.Format("Linking {0}...", szOutPattern));

			string output = LaunchProcWithArgs(szProcAndArgs);
			if (cmdLine.Verbose)
			{
				output = output.Trim();
				output = output.Replace("\r", "");
				output = output.Replace("\n", "");
				if (output != String.Empty)
					Console.WriteLine(output);
			}
		}

		string GetIntFileName(string _srcFile)
		{
			string szIntPattern   = GetProjectProperty("IntPattern");
			string szFileDir      = System.IO.Path.GetDirectoryName(_srcFile);
			string szFileNameOnly = System.IO.Path.GetFileNameWithoutExtension(_srcFile);
			string szIntFile      = System.IO.Path.Combine(szFileDir, szFileNameOnly);

			szIntFile = szIntFile.Replace('/', '.');
			szIntFile = szIntFile.Replace('\\', '.');
			string szIntFileName = ResolveSpecificToken(szIntPattern, szIntFile);

			return szIntFileName;
		}

		void CompileBuildItem(BuildItem _in, string _szIntFileName)
		{
			string szCompiler       = GetProjectProperty("Compiler");
			string szCommonOpts     = GetOverridableProperty("CommonOptions");
			string szCompileOpts    = GetOverridableProperty("CompileOptions");
			string szGenerateInOpts = GetOverridableProperty("GenerateIntOptions");
			string szProcAndArgs    = String.Format("{0} {1} {2}", szCompiler, szCommonOpts, szCompileOpts);

			string szFileToCompile = _in.SrcFile;
			string procAndArgs     = String.Format("{0} {1} {2} {3}", szProcAndArgs, szFileToCompile, szGenerateInOpts, _szIntFileName);

			Console.WriteLine(String.Format("Compiling {0}...", szFileToCompile));
			string output = LaunchProcWithArgs(procAndArgs);

			if (cmdLine.Verbose)
			{
				output = output.Trim();
				output = output.Replace("\r", "");
				output = output.Replace("\n", "");
				if (output != String.Empty)
					Console.WriteLine(output);
			}

			GenerateDepFile(_in);
		}

		private string ResolveGenericTokens(string _in)
		{
			string resolved = _in;

			// Loop until all tokens have been updated
			bool changed = false;
			do
			{
				changed = false;
				foreach (Tuple<string, string> token in genericTokens)
				{
					string oldStr = resolved;
					string newStr = oldStr.Replace(token.Item1, token.Item2);
					changed |= String.Compare(oldStr, newStr) != 0;

					resolved = newStr;
				}
			}
			while (changed);

			return resolved;
		}

		private string ResolveSpecificToken(string _in, string _fileName)
		{
			string resolved = ResolveGenericTokens(_in);
			resolved = resolved.Replace("{$Filename}", _fileName);
			return resolved;
		}

		private void CleanDeps()
		{
			string szDepsDir = GetProjectProperty("DepsDir");

			if (cmdLine.Verbose)
				Console.WriteLine(String.Format("Cleaning dependencies: {0}", szDepsDir));

			if (Directory.Exists(szDepsDir))
				Directory.Delete(szDepsDir, true);
		}

		private void CleanIntFiles()
		{
			string szIntDir = GetProjectProperty("IntDir");

			if (cmdLine.Verbose)
				Console.WriteLine(String.Format("Cleaning intermediate directory: {0}", szIntDir));

			if (Directory.Exists(szIntDir))
				Directory.Delete(szIntDir, true);
		}

		private BuildItem FetchDependencies(string srcFile)
		{
			string szCompiler    = GetProjectProperty("Compiler");
			string szCommonOpts  = GetOverridableProperty("CommonOptions");
			string szCompileOpts = GetOverridableProperty("CompileOptions");
			string szGenDepOpts  = GetOverridableProperty("GenerateDepsOptions");
			string szProcAndArgs = String.Format("{0} {1} {2} {3}", szCompiler, szCommonOpts, szCompileOpts, szGenDepOpts);

			ConcurrentBag<BuildItem> depList = new ConcurrentBag<BuildItem>();
			
			if (cmdLine.Verbose)
				Console.WriteLine(String.Format("Fetching dependencies of {0}...", srcFile));

			BuildItem bi = new BuildItem()
			{
				SrcFile = srcFile
			};

			string procAndArgs = String.Format("{0} {1}", szProcAndArgs, srcFile);
			string output      = LaunchProcWithArgs(procAndArgs);

			// output has format:
			// Filename.o : path\to\input\Filename.cpp path\to/header/file.h \
			//  path\to/other/header/file.h \
			//  path\to/other/other/header_file.h

			int skip = output.IndexOf(": ") + 2;
			output = output.Substring(skip);

			foreach (string depFile in output.Split(new[] { " \\", "\r\n", "\r", "\n", " " }, StringSplitOptions.RemoveEmptyEntries))
			{
				string sanitized = depFile;
				if (sanitized.EndsWith("\\"))
					sanitized = sanitized.Substring(0, sanitized.Length - 1);

				sanitized = sanitized.Trim(new[] { ' ', '\t' });
				sanitized = sanitized.Replace('/', '\\');

				BuildItem.Dependency biDep = new BuildItem.Dependency()
				{
					FilePath = sanitized,
					LastWriteTime = File.GetLastWriteTime(sanitized)
				};
				bi.DepList.Add(biDep);
			}

			return bi;
		}

		private List<string> FetchFilesToBuild()
		{
			string szSrcDir  = GetProjectProperty("SrcDir");
			string szPattern = GetProjectProperty("SrcPattern");
			SearchOption opts = Root.ProjectProperties.SrcRecurse.Value ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
			if (cmdLine.Verbose)
				Console.WriteLine(String.Format("Fetching files to build: path={0}, pattern={1}, recurse={2}", szSrcDir, szPattern, Root.ProjectProperties.SrcRecurse));

			string[] fileArray = Directory.GetFiles(szSrcDir, szPattern, opts);

			if (cmdLine.Verbose)
				Console.WriteLine(String.Format("Found {0} files.", fileArray.Length));

			return fileArray.ToList();
		}

		private void ResolveTarget()
		{
			string wantedTarget = cmdLine.Target;
			currentTarget = Root.Targets.FirstOrDefault(x => x.Name == wantedTarget);
			if (currentTarget == null)
				throw new Exception(String.Format("Target {0} not found in {1}", wantedTarget, Path));

			SetupTokens();
		}

		private void SetupTokens()
		{
			// Setup a token for each ProjectProperties
			PropertyInfo[] projectProperties = Root.ProjectProperties.GetType().GetProperties();
			foreach (PropertyInfo curProp in projectProperties)
			{
				string propName = String.Format("{{${0}}}", curProp.Name);
				string propValue = curProp.GetValue(Root.ProjectProperties) as string;
				Tuple<string, string> curToken = new Tuple<string, string>(propName, propValue);
				genericTokens.Add(curToken);
			}

			// Setup a token for the default options
			PropertyInfo[] defaultOptsProperties = Root.DefaultOptions.GetType().GetProperties();
			foreach (PropertyInfo curProp in defaultOptsProperties)
			{
				string propName = String.Format("{{$DefaultOptions.{0}}}", curProp.Name);
				string propValue = curProp.GetValue(Root.DefaultOptions) as string;
				Tuple<string, string> curToken = new Tuple<string, string>(propName, propValue);
				genericTokens.Add(curToken);
			}

			// Setup a token for all targets
			foreach (Target t in Root.Targets)
			{
				string targetName = t.Name;
				PropertyInfo[] targetProperies = t.GetType().GetProperties();
				foreach (PropertyInfo curProp in targetProperies)
				{
					string propName = String.Format("{{${0}.{1}}}", targetName, curProp.Name);
					string propValue = curProp.GetValue(t) as string;
					Tuple<string, string> curToken = new Tuple<string, string>(propName, propValue);
					genericTokens.Add(curToken);
				}
			}

			// Setup a token for current target name
			genericTokens.Add(new Tuple<string, string>("{$Target}", currentTarget.Name));
		}

		private void SetProjDir()
		{
			string busyProjDir = System.IO.Path.GetDirectoryName(Path);
			string szProjDir   = GetProjectProperty("ProjDir");
			string szWorkDir   = System.IO.Path.Combine(busyProjDir, szProjDir);
			szWorkDir = System.IO.Path.GetFullPath(szWorkDir);

			if (cmdLine.Verbose)
				Console.WriteLine("Setting proj dir: {0}", szWorkDir);

			Directory.SetCurrentDirectory(szWorkDir);
		}

		private void PreBuildStep()
		{
			string szPreBuildStep = GetOverridableProperty("PreBuildStep");
			if (szPreBuildStep == null)
				return;

			if (cmdLine.Verbose)
				Console.WriteLine("Executing PreBuildStep: {0}", szPreBuildStep);

			string output = LaunchProcWithArgs(szPreBuildStep);
			Console.WriteLine(output); 
		}

		private void PostBuildStep()
		{
			string szPostBuildStep = GetOverridableProperty("PostBuildStep");
			if (szPostBuildStep == null)
				return;

			if (cmdLine.Verbose)
				Console.WriteLine("Executing PostBuildStep: {0}", szPostBuildStep);

			string output = LaunchProcWithArgs(szPostBuildStep);
			Console.WriteLine(output);
		}

		private string GetProjectProperty(string _prop)
		{
			string propVal = Root.ProjectProperties.GetType().GetProperty(_prop).GetValue(Root.ProjectProperties, null) as string;
			propVal = ResolveGenericTokens(propVal);
			return propVal;
		}

		private string GetOverridableProperty(string _prop)
		{
			string propVal = currentTarget.GetType().GetProperty(_prop).GetValue(currentTarget, null) as string;
			if (propVal == null)
				propVal = Root.DefaultOptions.GetType().GetProperty(_prop).GetValue(Root.DefaultOptions, null) as string;

			if (propVal != null)
				propVal = ResolveGenericTokens(propVal);

			return propVal;
		}

		private string LaunchProcWithArgs(string procAndArgs)
		{
			using (Process p = new Process())
			{
				p.StartInfo.UseShellExecute = false;
				//p.StartInfo.FileName = procAndArgs.First();
				//p.StartInfo.Arguments = (procAndArgs.Count() > 1) ? procAndArgs.Skip(1).Aggregate((x, y) => x + " " + y) : String.Empty;
				p.StartInfo.FileName = "cmd.exe";
				p.StartInfo.Arguments = String.Format("/C \"{0}\"", String.Join(" ", procAndArgs));
				p.StartInfo.CreateNoWindow = true;
				p.StartInfo.RedirectStandardOutput = true;
				p.StartInfo.RedirectStandardError = true;
				p.Start();

				string stdErr = string.Empty;
				string stdOut = string.Empty;
				while (!p.HasExited)
				{
					stdErr += p.StandardError.ReadToEnd();
					stdOut += p.StandardOutput.ReadToEnd();
				}
				stdErr += p.StandardError.ReadToEnd();
				stdOut += p.StandardOutput.ReadToEnd();
				p.WaitForExit();

				if (p.ExitCode != 0)
					throw new Exception(String.Format("{0} exited with code {1} : {2}", p.StartInfo.FileName, p.ExitCode, stdErr));

				return stdOut;
			}
		}
	}
}
