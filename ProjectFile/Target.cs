﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BuSy.ProjectFile
{
	public class Target
	{
		public string Name { get; set; }

		[XmlElement("PreBuildStep", IsNullable = true)]
		public string PreBuildStep { get; set; }
		[XmlElement("PostBuildStep", IsNullable = true)]
		public string PostBuildStep { get; set; }

		[XmlElement("CommonOptions", IsNullable = true)]
		public string CommonOptions { get; set; }
		[XmlElement("GenerateDepsOptions", IsNullable = true)]
		public string GenerateDepsOptions { get; set; }
		[XmlElement("GenerateIntOptions", IsNullable = true)]
		public string GenerateIntOptions { get; set; }
		[XmlElement("GenerateOutOptions", IsNullable = true)]
		public string GenerateOutOptions { get; set; }
		[XmlElement("CompileOptions", IsNullable = true)]
		public string CompileOptions { get; set; }
		[XmlElement("LinkOptions", IsNullable = true)]
		public string LinkOptions { get; set; }

		[XmlElement("AssemblyName", IsNullable = true)]
		public string AssemblyName { get; set; }
	}
}
