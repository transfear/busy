﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BuSy.ProjectFile
{
	public class Root
	{
		public ProjectProperties ProjectProperties { get; set; }

		[XmlElement("DefaultOptions", IsNullable = true)]
		public DefaultOptions DefaultOptions { get; set; } = new DefaultOptions();

		public Target[] Targets { get; set; }
	}
}
