﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuSy.Utils
{
	static internal class StringUtils
	{
		// Splits a string containing an executable and its arguments
		public static List<string> SplitExecAndArgs(string _in)
		{
			// Source https://stackoverflow.com/questions/29827049/c-sharp-split-executable-path-and-arguments-into-two-strings

			List<string> allLineFields = new List<string>();
			var textStream = new System.IO.StringReader(_in);
			using (var parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(textStream))
			{
				parser.Delimiters = new string[] { " " };
				parser.HasFieldsEnclosedInQuotes = true; // <--- !!!
				string[] fields;
				while ((fields = parser.ReadFields()) != null)
				{
					allLineFields.AddRange(fields);
				}
			}

			return allLineFields;
		}
	}
}
