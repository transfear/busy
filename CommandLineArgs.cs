﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuSy
{
	internal class CommandLineArgs
	{
		public string[] Args { get; set; }

		public string ProjPath { get; set; }
		public bool   Verbose  { get; set; } = false;
		public Rule   Rule     { get; set; } = Rule.Build;
		public string Target   { get; set; } = "Debug";

		public CommandLineArgs(string[] _args)
		{
			Args = _args;
			Parse();
		}

		private void Parse()
		{
			int numArgs = Args.Length;
			int argIdx  = 0;
			do
			{
				string curArg = Args[argIdx];
				if (curArg.StartsWith("-"))
					argIdx += ParseOption(argIdx, curArg.ToLower());
				else
					ProjPath = curArg;

				++argIdx;
			}
			while (argIdx < numArgs);
		}

		private int ParseOption(int _optIdx, string _opt)
		{
			switch(_opt)
			{
				case "-v":
				case "-verbose":
					{
						Verbose = true;
						return 0;
					}

				case "-b":
				case "-build":
					{
						Rule = Rule.Build;
						break;
					}
				case "-r":
				case "-rebuild":
					{
						Rule = Rule.Rebuild;
						break;
					}
				case "-c":
				case "-clean":
					{
						Rule = Rule.Clean;
						break;
					}

				case "-t":
					{
						Target = Args[_optIdx + 1];
						return 1;
					}

				default:
					{
						throw new Exception(String.Format("Unknown option {0}", _opt));
					}
			}

			return 0;
		}
	}
}
